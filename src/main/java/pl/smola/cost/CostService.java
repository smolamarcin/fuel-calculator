package pl.smola.cost;

import com.google.common.collect.ImmutableList;

/**
 * Route service allows you to retrieve informations and calculate price of the whole route based on addresses.
 */
public interface CostService {
    /**
     * Calculate cost of the route, between two addresses, based on vehicle name.
     *
     * @param src      - source address
     * @param dst      - destination address
     * @param vehicleName - name of the vehicle
     * @return
     */
    Price calculateRouteCost(String src, String dst, String vehicleName);

    /**
     * Calculate cost of the route, based on multiple addresses (waypoints) and on a given vehicle model name.
     * @param addresses - list of addresses
     * @param vehicleName - name of te vehicle
     * @return
     */
    Price calculateRouteCost(ImmutableList<String> addresses, String vehicleName);

}
