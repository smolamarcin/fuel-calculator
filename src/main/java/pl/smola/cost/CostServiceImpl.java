package pl.smola.cost;

import com.google.common.collect.ImmutableList;
import pl.smola.geo.Distance;
import pl.smola.vehicles.Vehicle;
import pl.smola.geo.DistanceCalculator;
import pl.smola.vehicles.VehicleRetriever;

import javax.inject.Inject;

class CostServiceImpl implements CostService {
    private final FuelPriceRetriever fuelPriceRetriever;
    private final DistanceCalculator distanceCalculator;
    private final RouteCostCalculator routeCostCalculator;
    private final VehicleRetriever vehicleRetriever;
    @Inject
    CostServiceImpl(FuelPriceRetriever fuelPriceRetriever,
                    DistanceCalculator distanceCalculator,
                    RouteCostCalculator routeCostCalculator, VehicleRetriever vehicleRetriever) {
        this.fuelPriceRetriever = fuelPriceRetriever;
        this.distanceCalculator = distanceCalculator;
        this.routeCostCalculator = routeCostCalculator;
        this.vehicleRetriever = vehicleRetriever;
    }


    @Override
    public Price calculateRouteCost(String src, String dst, String vehicleName) {
        Distance distance = distanceCalculator.calculate(src, dst);
        Vehicle vehicle = vehicleRetriever.retrieveInfo(vehicleName);
        Price actualFuelPrice = fuelPriceRetriever.retrieve(vehicle.getFuelType());
        return routeCostCalculator.calculate(distance, vehicle, actualFuelPrice);

    }

    @Override
    public Price calculateRouteCost(ImmutableList<String> addresses, String vehicleName) {
        Distance distance = distanceCalculator.calculate(addresses);
        Vehicle vehicle = vehicleRetriever.retrieveInfo(vehicleName);
        Price actualFuelPrice = fuelPriceRetriever.retrieve(vehicle.getFuelType());
        return routeCostCalculator.calculate(distance, vehicle, actualFuelPrice);
    }

}
