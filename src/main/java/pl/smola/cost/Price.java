package pl.smola.cost;

import com.google.auto.value.AutoValue;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.util.Locale;

@AutoValue
public abstract class Price {
    /**
     * Default currency for calculations.
     */
    private static final CurrencyUnit DEFAULT_CURRENCY = Monetary.getCurrency(Locale.US);

    public abstract MonetaryAmount getAmount();

    public CurrencyUnit getCurrencyUnit() {
        return getAmount().getCurrency();
    }

    public CurrencyUnit getDefaultCurrency() {
        return DEFAULT_CURRENCY;
    }

    public static Price create(Number amount) {
        return new pl.smola.cost.AutoValue_Price(Monetary.getDefaultAmountFactory()
                .setNumber(amount)
                .setCurrency(Price.DEFAULT_CURRENCY)
                .create());
    }

    public static Price create(Number amount, String currencyCode) {
        return new pl.smola.cost.AutoValue_Price(Monetary.getDefaultAmountFactory()
                .setNumber(amount)
                .setCurrency(Monetary.getCurrency(currencyCode.toUpperCase()))
                .create());
    }



    public Price multiply(Number value) {
        MonetaryAmount multiply = getAmount().multiply(value);
        return Price.create(multiply);
    }

    private static Price create(MonetaryAmount monetaryAmount) {
        return new pl.smola.cost.AutoValue_Price(monetaryAmount);
    }


}
