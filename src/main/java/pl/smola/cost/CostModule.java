package pl.smola.cost;

import com.google.inject.Binder;
import com.google.inject.Module;
import pl.smola.geo.GeoModule;
import pl.smola.vehicles.VehicleModule;

public class CostModule implements Module {
    @Override
    public void configure(Binder binder) {
        binder.bind(CostService.class).to(CostServiceImpl.class);
        binder.bind(RouteCostCalculator.class).to(RouteCostCalculatorImpl.class);
        binder.bind(FuelPriceRetriever.class).to(FuelPriceRetrieverImpl.class);

        // other modules
        binder.install(new GeoModule());
        binder.install(new VehicleModule());
    }
}
