package pl.smola.cost;

import pl.smola.geo.Distance;
import pl.smola.vehicles.Vehicle;

public interface RouteCostCalculator {
    Price calculate(Distance distance, Vehicle car, Price fuelPrice);
}
