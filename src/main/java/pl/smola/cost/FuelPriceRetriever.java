package pl.smola.cost;

import pl.smola.vehicles.FuelType;

public interface FuelPriceRetriever {
    Price retrieve(FuelType fuelType);
}
