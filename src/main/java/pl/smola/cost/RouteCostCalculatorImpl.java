package pl.smola.cost;

import pl.smola.geo.Distance;
import pl.smola.vehicles.Vehicle;

class RouteCostCalculatorImpl implements RouteCostCalculator {
    private static final int KILOMETERS_FACTOR = 1000;
    private static final int FUEL_CONSUMPTION_FACTOR = 100;

    @Override
    public Price calculate(Distance distance, Vehicle vehicle, Price fuelPrice) {
        long kilometers = distance.inMeters() / KILOMETERS_FACTOR;
        int fuelConsumption = vehicle.getFuelConsumption();
        long usedFuel = (kilometers * fuelConsumption) / FUEL_CONSUMPTION_FACTOR;
        return fuelPrice.multiply(usedFuel);
    }
}
