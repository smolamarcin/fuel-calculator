package pl.smola;

import com.google.inject.Guice;
import pl.smola.vehicles.VehicleModule;

class App {

    public static void main(String[] args) {
        com.google.inject.Injector injector = Guice.createInjector(new VehicleModule());
        injector.injectMembers(new App());
    }
}
