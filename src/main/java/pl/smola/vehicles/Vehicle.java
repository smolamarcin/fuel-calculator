package pl.smola.vehicles;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Vehicle {
    public abstract String getName();

    public abstract int getFuelConsumption();

    public abstract FuelType getFuelType();

    public abstract VehicleType getVehicleType();

    public static Vehicle create(String newName, int newFuelConsumption, FuelType newFuelType, VehicleType newVehicleType) {
        return builder()
                .setName(newName)
                .setFuelConsumption(newFuelConsumption)
                .setFuelType(newFuelType)
                .setVehicleType(newVehicleType)
                .build();
    }

    public static VehicleBuilder builder() {
        return new pl.smola.vehicles.AutoValue_Vehicle.Builder();
    }


    @AutoValue.Builder
    public abstract static class VehicleBuilder {
        public abstract VehicleBuilder setName(String newName);

        public abstract VehicleBuilder setFuelConsumption(int newFuelConsumption);

        public abstract VehicleBuilder setFuelType(FuelType newFuelType);

        public abstract VehicleBuilder setVehicleType(VehicleType newVehicleType);

        public abstract Vehicle build();
    }
}
