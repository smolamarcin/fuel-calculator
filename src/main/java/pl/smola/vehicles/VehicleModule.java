package pl.smola.vehicles;

import com.google.inject.Binder;
import com.google.inject.Module;

public class VehicleModule implements Module {
    @Override
    public void configure(Binder binder) {
        binder.bind(VehicleRetriever.class).to(HttpRetriever.class);
    }
}
