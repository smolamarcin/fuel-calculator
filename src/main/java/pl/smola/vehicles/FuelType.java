package pl.smola.vehicles;

public enum FuelType {
    PB_95, PB_98, DIESEL
}
