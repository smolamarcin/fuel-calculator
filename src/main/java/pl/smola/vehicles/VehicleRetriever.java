package pl.smola.vehicles;

public interface VehicleRetriever {
    Vehicle retrieveInfo(String vehicleName);
}
