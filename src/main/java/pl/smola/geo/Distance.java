package pl.smola.geo;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Distance {
    public abstract long inMeters();

    public static Distance create(long inMeters) {
        return new pl.smola.geo.AutoValue_Distance(inMeters);
    }
}
