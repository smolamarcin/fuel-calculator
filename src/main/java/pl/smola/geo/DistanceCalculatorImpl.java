package pl.smola.geo;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;

class DistanceCalculatorImpl implements DistanceCalculator {
    private final GoogleApiConfig apiConfig;
    private static final Logger LOGGER = LoggerFactory.getLogger(DistanceCalculatorImpl.class);

    @Inject
    DistanceCalculatorImpl(GoogleApiConfig context) {
        this.apiConfig = context;
    }

    @Override
    public Distance calculate(String src, String dst) {
        if (src.equalsIgnoreCase(dst)) {
            throw new IllegalArgumentException("Origin must be different that destination!");
        }
        try {
            DistanceMatrix response = DistanceMatrixApi.newRequest(apiConfig.getGeoApiContext())
                    .origins(src)
                    .destinations(dst)
                    .await();
            return Distance.create(response
                    .rows[0]
                    .elements[0]
                    .distance.inMeters);
        } catch (ApiException | InterruptedException | IOException e) {
            LOGGER.error(e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
