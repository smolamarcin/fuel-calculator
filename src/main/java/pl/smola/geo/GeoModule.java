package pl.smola.geo;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Singleton;

public class GeoModule implements Module {
    private final String googleApiConfigPath = "src/main/resources/google-api.properties";

    @Override
    public void configure(Binder binder) {
        binder.bindConstant().annotatedWith(GoogleAPIFileName.class).to(googleApiConfigPath);
        binder.bind(DistanceCalculator.class).to(DistanceCalculatorImpl.class);
        binder.bind(GeoCoordinatesTranslator.class).to(GeoCoordinatesTranslatorImpl.class);
        binder.bind(GoogleApiConfig.class).toProvider(GoogleApiConfigProvider.class).in(Singleton.class);
    }


}
