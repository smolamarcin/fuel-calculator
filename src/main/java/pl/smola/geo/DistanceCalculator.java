package pl.smola.geo;

import com.google.common.collect.ImmutableList;

public interface DistanceCalculator {
    Distance calculate(String src, String dst);

    default Distance calculate(ImmutableList<String> addresses)  {
        Distance totalDistance = Distance.create(0);
        for (int i = 0; i < addresses.size() - 1; i++) {
            String currentWaypoint = addresses.get(i);
            String nextWaypoint = addresses.get(i + 1);
            totalDistance = Distance.create(totalDistance.inMeters()
                    + this.calculate(currentWaypoint, nextWaypoint).inMeters());
        }
        return totalDistance;
    }
}
