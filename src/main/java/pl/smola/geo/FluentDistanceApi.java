package pl.smola.geo;

import com.google.inject.Inject;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;

import java.io.IOException;

public class FluentDistanceApi {
    private GoogleApiConfig googleApiConfig;

    @Inject
    public FluentDistanceApi(GoogleApiConfig googleApiConfig) {
        this.googleApiConfig = googleApiConfig;
    }

    public FromStep request() {
        return new FluentDistanceApiBuilder(this.googleApiConfig);
    }

    public class FluentDistanceApiBuilder implements FromStep, ToStep, CalculateResult {
        private String src;
        private String dst;
        private GoogleApiConfig googleApiConfig;

        private FluentDistanceApiBuilder(GoogleApiConfig googleApiConfig) {
            this.googleApiConfig = googleApiConfig;
        }


        @Override
        public ToStep from(String src) {
            this.src = src;
            return this;
        }

        @Override
        public CalculateResult to(String dst) {
            this.dst = dst;
            return this;
        }

        @Override
        public Distance calculate() {
            if (src.equalsIgnoreCase(dst)) {
                throw new IllegalArgumentException("Origin must be different than destination!");
            }
            try {
                DistanceMatrix response = DistanceMatrixApi.newRequest(googleApiConfig.getGeoApiContext())
                        .origins(src)
                        .destinations(dst)
                        .await();
                return Distance.create(response
                        .rows[0]
                        .elements[0]
                        .distance.inMeters);
            } catch (ApiException | InterruptedException | IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    public interface FromStep {
        ToStep from(String src);
    }

    public interface ToStep {
        CalculateResult to(String dst);
    }

    public interface CalculateResult {
        Distance calculate();
    }
}


