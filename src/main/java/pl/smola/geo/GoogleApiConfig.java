package pl.smola.geo;

import com.google.maps.GeoApiContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

class GoogleApiConfig {
    private static GeoApiContext geoApiContext;
    private static final String API_KEY_PROPERTY = "api-key";
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleApiConfig.class);

    GoogleApiConfig(String filePath) {
        initialize(filePath);
    }

    void initialize(String filePath) {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(filePath));
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        geoApiContext = new GeoApiContext.Builder()
                .apiKey(properties.getProperty(API_KEY_PROPERTY))
                .build();

    }

    GeoApiContext getGeoApiContext() {
        return geoApiContext;
    }
}
