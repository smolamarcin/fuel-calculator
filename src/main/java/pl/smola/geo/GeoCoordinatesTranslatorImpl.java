package pl.smola.geo;

import com.google.inject.Inject;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;

class GeoCoordinatesTranslatorImpl implements GeoCoordinatesTranslator {
    private final Logger logger = LoggerFactory.getLogger(GeoCoordinatesTranslatorImpl.class);
    private final GoogleApiConfig apiConfig;

    @Inject
    GeoCoordinatesTranslatorImpl(GoogleApiConfig apiConfig) {
        this.apiConfig = apiConfig;
    }

    public GeoCoordinates translate(String address) {
        try {
            GeocodingResult[] resultMatrix = GeocodingApi
                    .geocode(apiConfig.getGeoApiContext(), address)
                    .await();
            LatLng geometry = resultMatrix[0]
                    .geometry.location;
            return GeoCoordinates.builder()
                    .setLongitude(BigDecimal.valueOf(geometry.lng))
                    .setLatitude(BigDecimal.valueOf(geometry.lat))
                    .build();
        } catch (ApiException | InterruptedException | IOException e) {
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }


}
