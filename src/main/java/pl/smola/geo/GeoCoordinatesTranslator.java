package pl.smola.geo;

import com.google.common.collect.ImmutableList;

import java.util.List;

import static com.google.common.collect.ImmutableList.toImmutableList;

public interface GeoCoordinatesTranslator {
    GeoCoordinates translate(String address);

    default ImmutableList<GeoCoordinates> translate(List<String> addresses) {
        return addresses.stream()
                .map(this::translate)
                .collect(toImmutableList());
    }
}
