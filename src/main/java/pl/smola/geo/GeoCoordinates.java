package pl.smola.geo;

import com.google.auto.value.AutoValue;

import java.math.BigDecimal;

import static com.google.maps.internal.ratelimiter.Preconditions.checkArgument;

@AutoValue
public abstract class GeoCoordinates {
    public abstract BigDecimal getLongitude();

    public abstract BigDecimal getLatitude();

    public static Builder builder() {
        return new pl.smola.geo.AutoValue_GeoCoordinates.Builder();
    }


    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder setLongitude(BigDecimal newLongitude);

        public abstract Builder setLatitude(BigDecimal newLatitude);

        abstract GeoCoordinates autoBuild();

        abstract BigDecimal getLongitude();

        abstract BigDecimal getLatitude();

        public GeoCoordinates build() {
            BigDecimal latitude = getLatitude().setScale(4, BigDecimal.ROUND_DOWN);
            validateLatitude(latitude);
            setLatitude(latitude);
            BigDecimal longitude = getLongitude().setScale(4, BigDecimal.ROUND_DOWN);
            validateLongitude(longitude);
            setLongitude(longitude);
            return autoBuild();
        }

        private void validateLongitude(BigDecimal longitude) {
            checkArgument(longitude.compareTo(BigDecimal.valueOf(-180.0001)) > 0 &&
                    longitude.compareTo(BigDecimal.valueOf(180.0001)) < 0, "Longitude must be between -180 and 180.");
        }


        private void validateLatitude(BigDecimal latitude) {
            checkArgument(latitude.compareTo(BigDecimal.valueOf(-90.0001)) > 0 &&
                    latitude.compareTo(BigDecimal.valueOf(90.0001)) < 0, "Latitude must be between -90 and 90.");
        }
    }
}
