package pl.smola.geo;

import com.google.inject.Inject;
import com.google.inject.Provider;

class GoogleApiConfigProvider implements Provider<GoogleApiConfig> {
    private final String filePath;

    @Inject
    GoogleApiConfigProvider(@GoogleAPIFileName String filePath) {
        this.filePath = filePath;
    }

    @Override
    public GoogleApiConfig get() {
        return new GoogleApiConfig(filePath);
    }
}
