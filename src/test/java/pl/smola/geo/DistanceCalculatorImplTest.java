package pl.smola.geo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

public class DistanceCalculatorImplTest {
    private DistanceCalculatorImpl distanceCalculator;
    private GoogleApiConfig apiConfig;

    @BeforeEach
    public void setUp() {
        apiConfig = mock(GoogleApiConfig.class, (Answer) invocation -> {
            throw new Exception("Something went wrong...");
        });
        distanceCalculator = new DistanceCalculatorImpl(apiConfig);
    }

    @Test
    void shouldThrowException_ForTwoSameAddresses() {
        // given
        String src = "Address";
        String dst = "Address";

        // when
        Exception exception = assertThrows(Exception.class,
                () -> distanceCalculator.calculate(src, dst));

        // then
        assertThat(exception).hasMessageThat().contains("Origin must be different that destination!");
    }

    @Test
    void shouldThrowException_whenRequestIsBad() {
        assertThrows(Exception.class, () -> distanceCalculator.calculate("as", "asd"));
    }

}
