package pl.smola.geo;

import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DistanceCalculatorImplTestIT {
    private DistanceCalculatorImpl distanceCalculator;

    @Test
    void shouldThrowException_whenApiKeyIsInvalid() {
        // given
        distanceCalculator = new DistanceCalculatorImpl(new GoogleApiConfig("src/test/resources/test.properties"));

        // when - then
        RuntimeException expectedException = assertThrows(RuntimeException.class, () -> distanceCalculator.calculate("firt address", "second address"));
        assertThat(expectedException).hasMessageThat().contains("Invalid API key.");
    }

}