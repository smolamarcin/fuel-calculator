package pl.smola.geo;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GeoCoordinatesTest {
    @Test
    void shouldCreateGeooCordinates_withProperScale() {
        // given
        GeoCoordinates geoCoordinate = GeoCoordinates.builder()
                .setLatitude(new BigDecimal(24.9991999999))
                .setLongitude(new BigDecimal(99.1111999999999))
                .build();

        // when - then
        assertThat(geoCoordinate.getLatitude()).isEquivalentAccordingToCompareTo(BigDecimal.valueOf(24.9991));
        assertThat(geoCoordinate.getLongitude()).isEquivalentAccordingToCompareTo(BigDecimal.valueOf(99.1111));
    }

    @Test
    void shouldThrowException_When_Latitude_OutOfRange() {
        IllegalArgumentException expectedException = assertThrows(IllegalArgumentException.class, () -> GeoCoordinates.builder()
                .setLatitude(new BigDecimal(-91))
                .setLongitude(new BigDecimal(180)).build());

        assertThat(expectedException).hasMessageThat().contains("Latitude must be between -90 and 90.");
    }

    @Test
    void shouldThrowException_When_Longitude_OutOfRange() {
        IllegalArgumentException expectedException = assertThrows(IllegalArgumentException.class, () -> GeoCoordinates.builder()
                .setLatitude(new BigDecimal(24))
                .setLongitude(new BigDecimal(181)).build());

        assertThat(expectedException).hasMessageThat().contains("Longitude must be between -180 and 180.");
    }

    @Test
    void equalsForSameObjects_shouldReturnTrue() {
        // given
        GeoCoordinates first = GeoCoordinates.builder().setLatitude(new BigDecimal(11.111111111)).setLongitude(new BigDecimal(99.9911)).build();
        GeoCoordinates second = GeoCoordinates.builder().setLatitude(new BigDecimal(11.1111222222)).setLongitude(new BigDecimal(99.99111111111111111111)).build();

        // when - then
        assertThat(first.equals(second)).isTrue();
    }

    @Test
    void equalsForSameObjects_withDifferentPrecision_shouldReturnTrue() {
        // given
        GeoCoordinates first = GeoCoordinates.builder().setLatitude(new BigDecimal(11.111111111)).setLongitude(new BigDecimal(99.9911)).build();
        GeoCoordinates second = GeoCoordinates.builder().setLatitude(new BigDecimal(11.11119999999)).setLongitude(new BigDecimal(99.9911999999999999999)).build();

        // when - then
        assertThat(first.equals(second)).isFalse();
    }

    @Test
    void shouldThrowException_whenOmitLatitude() {
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> GeoCoordinates.builder()
                .setLongitude(new BigDecimal(1))
                .build());
        assertThat(exception).hasMessageThat().contains("Property \"latitude\" has not been set");
    }

    @Test
    void shouldThrowException_whenOmitLongitude() {
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> GeoCoordinates.builder()
                .setLatitude(new BigDecimal(1))
                .build());
        assertThat(exception).hasMessageThat().contains("Property \"longitude\" has not been set");
    }

    @Test
    void shouldThrowException_whenTryingToAsingNullLatitude() {
        NullPointerException exception = assertThrows(NullPointerException.class, () -> {
            GeoCoordinates.builder()
                    .setLatitude(null)
                    .build();
        });
        assertThat(exception).hasMessageThat().contains("Null latitude");
    }
    @Test
    void shouldThrowException_whenTryingToAsingNullLongitude() {
        NullPointerException exception = assertThrows(NullPointerException.class, () -> {
            GeoCoordinates.builder()
                    .setLongitude(null)
                    .build();
        });
        assertThat(exception).hasMessageThat().contains("Null longitude");

    }

}