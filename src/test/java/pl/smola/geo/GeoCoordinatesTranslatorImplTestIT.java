package pl.smola.geo;

import com.google.maps.GeoApiContext;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.math.BigDecimal;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class GeoCoordinatesTranslatorImplTestIT {
    private static final String TEST_PROPERTIES = "src/test/resources/test.properties";
    private GeoCoordinatesTranslatorImpl geoCoordinatesTranslator;

    @Mock
    GoogleApiConfig googleApiConfig;

    @BeforeEach
    void setUp() {
        initMocks(this);
        geoCoordinatesTranslator = new GeoCoordinatesTranslatorImpl(new GoogleApiConfig(TEST_PROPERTIES));
    }

    @Test
    void shouldThrowException_whenTryingToSendRequest_withInvalidCredentials() {
        // given
        GeoApiContext geoApiContext = new GeoApiContext.Builder()
                .apiKey("invalid api key")
                .build();
        when(googleApiConfig.getGeoApiContext()).thenReturn(geoApiContext);

        // when - then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> geoCoordinatesTranslator.translate("address"));
        assertThat(exception).hasMessageThat().contains("Invalid API key.");
    }

    @Ignore // because api key in properties is not valid
    @Test
    void shouldRetrieveGeoCoordinates_basedOnProvidedAddress() {
        String address = "Wroclaw, ul. Piotra Skargi 1";
        double expectedLongitude = 99.87;
        double expectedLatitude = 22.54;

        // when
        GeoCoordinates result = geoCoordinatesTranslator.translate(address);

        // then
        assertThat(result).isNotNull();
        assertThat(result.getLongitude()).isEqualToIgnoringScale(new BigDecimal(expectedLongitude));
        assertThat(result.getLatitude()).isEqualToIgnoringScale(new BigDecimal(expectedLatitude));


    }

}