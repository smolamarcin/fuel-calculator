package pl.smola.cost;

import org.javamoney.moneta.spi.DefaultNumberValue;
import org.junit.jupiter.api.Test;

import javax.money.Monetary;
import javax.money.UnknownCurrencyException;
import java.util.Locale;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PriceTest {
    @Test
    void shouldCreatePrice_basedOnNumber_withProperCurrency() {
        // given
        double input = 24.99;

        // when
        Price price = Price.create(input, "USD");

        // then
        assertThat(price.getAmount().getNumber()).isEquivalentAccordingToCompareTo(new DefaultNumberValue(24.99));
        assertThat(price.getCurrencyUnit()).isEqualTo(Monetary.getCurrency(Locale.US));
    }

    @Test
    void shouldCreatePrice_basedOnNumber_withDefaultCurrency() {
        // given
        double input = 124.99;

        // when
        Price price = Price.create(input);

        // then
        assertThat(price.getAmount().getNumber()).isEquivalentAccordingToCompareTo(new DefaultNumberValue(124.99));
        assertThat(price.getCurrencyUnit()).isEqualTo(price.getDefaultCurrency());
    }

    @Test
    void equalsForPrice_withSameCurrency_shouldBeCaseInsensitive() {
        // given
        Price first = Price.create(11, "usd");
        Price second = Price.create(11, "USD");

        // when - then
        assertThat(first).isEqualTo(second);
    }

    @Test
    void equalsShouldReturnTrue_whenCreatedObject_withDifferentPrecisions() {
        // given
        Price basedOnDouble = Price.create(11.00, "usd");
        Price basedOnInt = Price.create(11, "usd");

        // when - then
        assertThat(basedOnDouble).isEqualTo(basedOnInt);
    }

    @Test
    void shouldThrowException_whenTryToCreate_PriceWithInvalidCurrency() {
        // given
        String invalidCurrency = "invalid currency";

        // when - then
        UnknownCurrencyException exception = assertThrows(UnknownCurrencyException.class, () -> {
            Price.create(12, invalidCurrency);
        });

        assertThat(exception).hasMessageThat().contains("Unknown currency code");
    }

    @Test
    void shouldThrowException_whenTryingToCreateInfinityPrice() {
        assertThrows(ArithmeticException.class, () -> Price.create(Double.POSITIVE_INFINITY));
    }
}