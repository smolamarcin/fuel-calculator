package pl.smola.cost;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import pl.smola.vehicles.FuelType;
import pl.smola.vehicles.Vehicle;
import pl.smola.geo.DistanceCalculator;
import pl.smola.vehicles.VehicleRetriever;
import pl.smola.vehicles.VehicleType;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class CostServiceImplTest {
    private CostServiceImpl routeService;

    @Mock
    private FuelPriceRetriever fuelPriceRetrieverMock;

    @Mock
    private DistanceCalculator distanceCalculatorMock;

    @Mock
    private RouteCostCalculator routeCostCalculatorMock;

    @Mock
    private VehicleRetriever vehicleRetrieverMock;

    @BeforeEach
    public void setUp() {
        initMocks(this);
        routeService = new CostServiceImpl(fuelPriceRetrieverMock, distanceCalculatorMock, routeCostCalculatorMock, vehicleRetrieverMock);
    }

    @Test
    void shouldThrowException_whenDistanceRequestFails() {
        // given
        String src = "Wroclaw";
        String dst = "Krakow";
        String vehicleModel = "Mercedes";
        when(distanceCalculatorMock.calculate(src, dst)).thenThrow(new RuntimeException("Something went wrong during calculating distance."));

        // when - then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> routeService.calculateRouteCost(src, dst, vehicleModel));

        assertThat(exception).hasMessageThat().contains("Something went wrong during calculating distance.");
    }

    @Test
    void shouldThrowException_whenVehicleRequestFails() {
        // given
        when(vehicleRetrieverMock.retrieveInfo(anyString())).thenThrow(new RuntimeException("Something went wrong during obtaining vehicle."));

        // when - then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> routeService.calculateRouteCost("", "", ""));
        assertThat(exception).hasMessageThat().contains("Something went wrong during obtaining vehicle.");
    }

    @Test
    void shouldThrowException_whenFuelPriceRequestFails() {
        // given
        when(vehicleRetrieverMock.retrieveInfo(anyString())).thenReturn(Vehicle.builder().setName("").setFuelConsumption(6)
                .setFuelType(FuelType.DIESEL).setVehicleType(VehicleType.CAR).build());
        when(fuelPriceRetrieverMock.retrieve(any(FuelType.class))).thenThrow(new RuntimeException("Cannot obtain fuel price"));

        // when - then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> routeService.calculateRouteCost("", "", ""));
        assertThat(exception).hasMessageThat().contains("Cannot obtain fuel price");

    }
}