package pl.smola.cost;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.smola.geo.Distance;
import pl.smola.vehicles.Vehicle;
import pl.smola.vehicles.VehicleType;

import static com.google.common.truth.Truth.assertThat;
import static pl.smola.vehicles.FuelType.DIESEL;

public class RouteCostCalculatorImplTest {
    private RouteCostCalculatorImpl routeCostCalculator;

    @BeforeEach
    public void setUp() {
        routeCostCalculator = new RouteCostCalculatorImpl();
    }

    @Test
    void shouldCalculateTotalPrice_basedOnInputArguments() {
        // given
        Distance oneKilometer = Distance.create(1_000_00);

        Vehicle car = Vehicle.builder().setName("Mercedes")
                .setFuelConsumption(6)
                .setFuelType(DIESEL)
                .setVehicleType(VehicleType.CAR)
                .build();
        Price actualFuelPrice = Price.create(5.21);

        // when
        Price totalRoutePrice = routeCostCalculator.calculate(oneKilometer, car, actualFuelPrice);

        // then
        assertThat(totalRoutePrice).isEqualTo(Price.create(31.26));


    }
}