package pl.smola;

import com.google.inject.Guice;
import org.junit.jupiter.api.Test;
import pl.smola.cost.CostModule;
import pl.smola.geo.GeoModule;
import pl.smola.vehicles.VehicleModule;

public class AppTestIT {
    @Test
    void shouldCreate_CostModule() {
        Guice.createInjector(new CostModule());
    }

    @Test
    void shouldCreate_GeoModule() {
        Guice.createInjector(new GeoModule());
    }

    @Test
    void shouldCreate_VehiclesModule() {
        Guice.createInjector(new VehicleModule());
    }
}